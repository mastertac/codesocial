# CodeSocial
**A social platform for software engineers and programmers.**

The project idea came from a struggle to find a platform dedicated to software engineers socializing with each other. I joked with my friend that such a platform doesn't exist because software engineers/programmers aren't social individuals.

Whether it's work related topics, joking around, exchanging information and ideas, or just discussing topics of interest, CodeSocial is the place for software engineers and programmers to socialize.