import 'react-native-url-polyfill/auto'
import { useState, useEffect } from 'react'
import { supabase } from './services/api/supabase'
import Auth from './services/authentication/Login'
import Account from './components/profile/Profile'
import { View } from 'react-native'
import { Session } from '@supabase/supabase-js'

export default function App() {
    const [session, setSession] = useState<Session | null>(null)

    useEffect(() => {
        supabase.auth.getSession().then(({ data: { session } }) => {
            setSession(session)
        })

        supabase.auth.onAuthStateChange((_event, session) => {
            setSession(session)
        })
    }, [])

    return (
        <View>
            {session && session.user ? <Account key={session.user.id} session={session} /> : <Auth />}
        </View>
    )
}