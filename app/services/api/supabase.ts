import 'react-native-url-polyfill/auto'
import AsyncStorage from '@react-native-async-storage/async-storage'
import { createClient } from '@supabase/supabase-js'

const supabaseUrl = "https://dlpzflgonppmunlkctfs.supabase.co"
const supabaseAnonKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6ImRscHpmbGdvbnBwbXVubGtjdGZzIiwicm9sZSI6ImFub24iLCJpYXQiOjE3MDczNTM1MDAsImV4cCI6MjAyMjkyOTUwMH0.pXFiYlkxZqB8z1lOZtO7n8pvnaE6Gu4lz1PZO1sVIAo"

export const supabase = createClient(supabaseUrl, supabaseAnonKey, {
    auth: {
        storage: AsyncStorage,
        autoRefreshToken: true,
        persistSession: true,
        detectSessionInUrl: false,
    },
})