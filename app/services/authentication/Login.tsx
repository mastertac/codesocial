import React, { useState } from 'react'
import { Alert, StyleSheet, View, AppState } from 'react-native'
import { supabase } from '../api/supabase'
import { Button, Input } from 'react-native-elements'
import SignIn from '../../components/sign_in/Sign_In'
import SignUp from '../../components/sign_up/Sign_Up'

AppState.addEventListener('change', (state) => {
    if (state === 'active') {
        supabase.auth.startAutoRefresh()
    } else {
        supabase.auth.stopAutoRefresh()
    }
})

export default function Auth() {
    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')
    const [showSignIn, setShowSignIn] = useState(true)
    const [loading, setLoading] = useState(false)

    const toggleForms = () => {
        setShowSignIn(!showSignIn)
    }

    const onChangeEmail = (text) => {
        setEmail(text)
    }

    const onChangePassword = (text) => {
        setPassword(text)
    }

    async function onSignInPress() {
        setLoading(true)
        try {
            const { error } = await supabase.auth.signInWithPassword({ email, password })
            if (error) Alert.alert(error.message)
        } catch (err) {
            Alert.alert(err.message)
        }
        setLoading(false)
    }

    return (
        <View style={styles.container}>
            {showSignIn ? (
                <SignIn
                    email={email}
                    password={password}
                    onSignInPress={onSignInPress}
                    onChangeEmail={onChangeEmail}
                    onChangePassword={onChangePassword}
                    loading={loading}
                />
            ) : (
                    <SignUp
                    loading={loading}
                        onGuestPress={() => console.log('Guest Account pressed')}
                        onUserPress={() => console.log('User Account pressed')}
                />
            )}
            <View style={styles.verticallySpaced}>
                <Button title={showSignIn ? 'Sign up' : 'Sign in'} disabled={loading} onPress={toggleForms} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
        padding: 12,
    },
    verticallySpaced: {
        paddingTop: 4,
        paddingBottom: 4,
        alignSelf: 'stretch',
    },
})