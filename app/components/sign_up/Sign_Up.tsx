import React from 'react';
import { StyleSheet, View, Text } from 'react-native';
import { Button, Input } from 'react-native-elements';

export default function SignUp({
    loading,
    onGuestPress,
    onUserPress,
}) {
    return (
        <View style={styles.container}>
            <View style={styles.horizontallySpaced}>
                <Button title="Guest Account" disabled={loading} onPress={onGuestPress} />
                <Text style={styles.text}>OR</Text>
                <Button title="User Account" disabled={loading} onPress={onUserPress} />
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginTop: 40,
    },
    horizontallySpaced: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingVertical: 15,
    },
    text: {
        fontSize: 16,
        color: '#888',
        marginHorizontal: 10,
    },
});